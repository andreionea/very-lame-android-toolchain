START:=$(shell date +%s)
ROOT_DIR:=${PWD}
TIMESCRIPT=./scripts/difftime.sh $(START)
PRINT_TIME_ELAPSED=if [ "$(lastword $(MAKECMDGOALS))" = "$@" ]; then \
	$(TIMESCRIPT); \
	fi

GDB_DEBUG_PORT:=5039
DEBUG_PORT:=8085

ENABLE_PCH:=1
ALL_DEPS:=
DEP_FILES:=
EXTERNAL_LIBS:= main jsoncpp libpng minizip libmicrohttpd assimp libjpeg zlib sol2 lua

include make_rules/clean_vars.mk

ifneq ($(words $(MAKECMDGOALS)),1)
.DEFAULT_GOAL = all
%:
	@$(MAKE) $@ --no-print-directory -rRf $(firstword $(MAKEFILE_LIST));\
	$(TIMESCRIPT)
else

include make_rules/pretty-print.mk
include make_rules/config.mk
include make_rules/toolchain_setter.mk

PATCHES_ORIG:=$(shell ls patches)
PATCHES_DEST:=$(patsubst %.patch, externals/%/modif.patch, $(PATCHES_ORIG))
PATCHED_LIBS:=$(patsubst %.patch, %, $(PATCHES_ORIG))

OBJDIR:=objs
DEPDIR:=deps
LIBDIR:=libs

-include $(addprefix defs/, $(addsuffix _defs.mk, $(EXTERNAL_LIBS)))

JAVA_FILES:=$(shell ls project/src/*.java)
CLASS_FILES:=$(foreach i,$(patsubst %.java,%.class,$(JAVA_FILES)), project/temp_output/classes_output/$(notdir $i))

################################End defines###################################
##############################################################################
################################Start rules###################################

all:build
externals/%/modif.patch: patches/%.patch
	@$(ECHO)${purple}Just patching up $* ${NC};\
	cd externals/$* ;\
	git reset HEAD --hard > /dev/null;\
	git apply --whitespace=nowarn ../../$^ > /dev/null;\
	cp ../../$^ modif.patch

prerequisites/%_cu.prereq:$(%_SRCS)
	@./scripts/create_cu_rules.py "$*" "$($*_SUBFOLDER)" "$($*_NCU)" "$($*_SRCS_FOR_CU)"

build:project/$(APK_NAME).apk

include make_rules/toolchain_rules.mk
include make_rules/java-rules.mk
include make_rules/solib_rules.mk

-include $(addprefix rules/, $(addsuffix _rules.mk, $(EXTERNAL_LIBS)))

ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),toolchain)
-include $(addprefix prerequisites/,$(addsuffix _cu.prereq, $(EXTERNAL_LIBS)))
# -include $(DEP_FILES)
endif
endif

include make_rules/adb-rules.mk
include make_rules/debug-rules.mk

clean:
	@git submodule foreach --recursive git reset HEAD --hard -q &> /dev/null
	@git submodule foreach --recursive git clean -xdf -q &> /dev/null
	@rm -f main/include/MainPCH.h.gch
	@rm -f main/src/android_native_app_glue.c main/include/android_native_app_glue.h
	@rm -rf $(OBJDIR)/ $(LIBDIR)/* $(DEPDIR)/*
	@rm -f prerequisites/* classes.dex
	@rm -rf project/bin project/temp_output project/lib project/*.apk project/*.apk_ project/AndroidManifest.xml
	@$(PRINT_TIME_ELAPSED)
$(ALL_DEPS):$(PATCHES_DEST)
update:
	git pull
endif
$(OBJDIR)/$(SUBFOLDER)$(LIBNAME)_cu%.o: $(OBJDIR)/$(SUBFOLDER)$(LIBNAME)_cu%.cpp $($(LIBNAME)_EXTRA_DEP) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_EXTRA_DEP))
	@$(ECHO) ${yellow}compiling CU $(notdir $<) containing `cat $< | wc -l` files ${NC}
	@mkdir -p $(dir $@)
	@$(CCACHE) $(CXX) $(CXXFLAGS) -I. $($(LIBNAME)_EXTRA_CXXFLAGS) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_INCLUDE_DIRS)) -c $< -o $@
$(OBJDIR)/$(SUBFOLDER)$(LIBNAME)_cu%.o: $(OBJDIR)/$(SUBFOLDER)$(LIBNAME)_cu%.c $($(LIBNAME)_EXTRA_DEP) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_EXTRA_DEP))
	@$(ECHO) ${yellow}compiling CU $(notdir $<) containing `cat $< | wc -l` files ${NC}
	@mkdir -p $(dir $@)
	@$(CCACHE) $(CC) $(CFLAGS) -I. $($(LIBNAME)_EXTRA_CFLAGS) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_INCLUDE_DIRS)) -c $< -o $@
$(OBJDIR)/$(SUBFOLDER)$(LIBNAME)/%.o: $(SUBFOLDER)$(LIBNAME)/%.c $($(LIBNAME)_EXTRA_DEP) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_EXTRA_DEP))
	@$(ECHO) ${green}compiling $(notdir $<) ${NC}
	@mkdir -p $(dir $@)
	@$(CCACHE) $(CC) $(CFLAGS) $($(LIBNAME)_EXTRA_CFLAGS) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_INCLUDE_DIRS)) -c $< -o $@
$(OBJDIR)/$(SUBFOLDER)$(LIBNAME)/%.o: $(SUBFOLDER)$(LIBNAME)/%.cpp $($(LIBNAME)_EXTRA_DEP) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_EXTRA_DEP))
	@$(ECHO) ${green}compiling $(notdir $<) ${NC}
	@mkdir -p $(dir $@)
	@$(CCACHE) $(CXX) $(CXXFLAGS) $($(LIBNAME)_EXTRA_CXXFLAGS) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_INCLUDE_DIRS)) -c $< -o $@
$(OBJDIR)/$(SUBFOLDER)$(LIBNAME)/%.o: $(SUBFOLDER)$(LIBNAME)/%.cc $($(LIBNAME)_EXTRA_DEP) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_EXTRA_DEP))
	@$(ECHO) ${green}compiling $(notdir $<) ${NC}
	@mkdir -p $(dir $@)
	@$(CCACHE) $(CXX) $(CXXFLAGS) $($(LIBNAME)_EXTRA_CXXFLAGS) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_EXTRA_DEP)) -c $< -o $@
libs/lib$(LIBNAME).a: $($(LIBNAME)_OBJS) $($(LIBNAME)_CU_OBJS)
	@$(ECHO) ${purple}Linking $@${NC}
	@$(AR) cru $@ $($(LIBNAME)_OBJS) $($(LIBNAME)_CU_OBJS)
	@$(PRINT_TIME_ELAPSED)
$(SUBFOLDER)$(LIBNAME)/%.h.gch: $(SUBFOLDER)$(LIBNAME)/%.h $($(LIBNAME)_PCH_DEP) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_EXTRA_DEP))
	@$(ECHO) ${green}precompiling $< to $@${NC}
	@$(CCACHE) $(CXX) $(CXXFLAGS) $($(LIBNAME)_PCH_CXXFLAGS) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_INCLUDE_DIRS)) $< -o $@


$(DEPDIR)/$(SUBFOLDER)$(LIBNAME)%.d: $(SUBFOLDER)$(LIBNAME)/%.cc $($(LIBNAME)_EXTRA_DEP) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_EXTRA_DEP))
	@mkdir -p $(dir $@)
	@$(CXX) $(CXXFLAGS) $($(LIBNAME)_EXTRA_CXXFLAGS) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_INCLUDE_DIRS)) -MM $< -MT $(OBJDIR)/$(SUBFOLDER)$(LIBNAME)/$*.o > $@
$(DEPDIR)/$(SUBFOLDER)$(LIBNAME)%.d: $(SUBFOLDER)$(LIBNAME)/%.cpp $($(LIBNAME)_EXTRA_DEP) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_EXTRA_DEP))
	@mkdir -p $(dir $@)
	@$(CXX) $(CXXFLAGS) $($(LIBNAME)_EXTRA_CXXFLAGS) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_INCLUDE_DIRS)) -MM $< -MT $(OBJDIR)/$(SUBFOLDER)$(LIBNAME)/$*.o > $@
$(DEPDIR)/$(SUBFOLDER)$(LIBNAME)_cu%.d: $(OBJDIR)/$(SUBFOLDER)$(LIBNAME)_cu%.cpp $($(LIBNAME)_EXTRA_DEP) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_EXTRA_DEP))
	@mkdir -p $(dir $@)
	@$(CXX) $(CXXFLAGS) $($(LIBNAME)_EXTRA_CXXFLAGS) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_INCLUDE_DIRS)) -MM $< -MT $(OBJDIR)/$(SUBFOLDER)$(LIBNAME)/$*.o > $@
$(DEPDIR)/$(SUBFOLDER)$(LIBNAME)/%.d: $(SUBFOLDER)$(LIBNAME)/%.c $($(LIBNAME)_EXTRA_DEP) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_EXTRA_DEP))
	@mkdir -p $(dir $@)
	@$(CC) $(CFLAGS) $($(LIBNAME)_EXTRA_CFLAGS) $(foreach dep,$($(LIBNAME)_TARGET_DEPS),$($(dep)_INCLUDE_DIRS)) -MM $< -MT $(OBJDIR)/$(SUBFOLDER)$(LIBNAME)/$*.o > $@
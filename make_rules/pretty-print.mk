# http://stackoverflow.com/a/455390 => inspiration for progress bar
ifndef ECHO
black='\033[0;30m'
dark_gray='\033[1;30m'
blue='\033[0;34m'
light_blue='\033[1;34m'
green='\033[0;32m'
light_green='\033[1;32m'
cyan='\033[0;36m'
light_cyan='\033[1;36m'
red='\033[0;31m'
light_red='\033[1;31m'
purple='\033[0;35m'
light_purple='\033[1;35m'
orange='\033[0;33m'
yellow='\033[1;33m'
light_gray='\033[0;37m'
white='\033[1;37m'
NC='\033[0m'

T := $(shell $(MAKE) $(MAKECMDGOALS) --no-print-directory \
      -nrRf $(firstword $(MAKEFILE_LIST)) \
      ECHO="COUNTTHIS" | grep -c "COUNTTHIS")
N := x
C = $(words $N)$(eval N := x $N)

ifeq ($(OS),Windows_NT)
ECHO = echo -e ${white} "`expr "  [\`expr $C '*' 100 / $T \`" : '.*\(....\)$$'`%]" ${NC}
else
ECHO = echo ${white} "`expr "  [\`expr $C '*' 100 / $T \`" : '.*\(....\)$$'`%]" ${NC}
endif

endif
project/mykey.keystore:
	@echo Creating release key
	@keytool -genkey -v -keystore project/mykey.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000

project/AndroidManifest.xml:android_templates/AndroidManifest.xml.template make_rules/config.mk
	@cat $< | sed s/PACKAGE_NAME/$(PACKAGE_NAME)/g | sed s/OUTPUT_LIB/$(OUTPUT_LIB)/g > $@

project/res/values/strings.xml:android_templates/strings.xml.template make_rules/config.mk
	@mkdir -p project/res/values
	@cat $< | sed s/PACKAGE_NAME/$(PACKAGE_NAME)/g | sed s/APP_NAME/"$(APP_NAME)"/g > $@

project/temp_output/R.java:project/assets/* project/res/values/strings.xml project/AndroidManifest.xml
	@$(AAPT) package -f  -m -S project/res -J project/temp_output -M project/AndroidManifest.xml -I $(ANDROID_SDK)/platforms/android-$(TARGET_PLATFORM)/android.jar;\
	mv project/temp_output/$(PACKAGE_DIR)/R.java $@; rm -rf project/temp_output/com

project/bin/pack-unsigned.apk:project/bin/classes.dex project/assets/* project/lib/armeabi-v7a/lib$(OUTPUT_LIB).so
	@$(ECHO) ${blue}Making unsigned APK${NC}
	@rm -f $@;\
	$(AAPT) package -f -M project/assets -M project/AndroidManifest.xml -A project/assets -S project/res --target-sdk-version $(TARGET_PLATFORM) -I $(ANDROID_SDK)/platforms/android-$(TARGET_PLATFORM)/android.jar -F $@ project/bin;\
	cd project/bin;\
	$(AAPT) add -f $@ classes.dex &> /dev/null;\
	cd ../../;\
	chmod 777 project/lib/armeabi-v7a/lib$(OUTPUT_LIB).so;\
	cd project; $(AAPT) add -f bin/pack-unsigned.apk lib/armeabi-v7a/lib$(OUTPUT_LIB).so > /dev/null

project/lib/armeabi-v7a/lib$(OUTPUT_LIB).so:$(LIBDIR)/stripped_lib$(OUTPUT_LIB).so
	@$(ECHO) ${blue}Copy lib${NC}
	@mkdir -p project/lib/armeabi-v7a ;\
	rm -f $@;\
	ln $< $@

project/bin/classes.dex:$(CLASS_FILES) project/temp_output/classes/$(PACKAGE_DIR)/R.class
	@$(ECHO) ${blue}Classes.dex ${NC}	
	@rm -rf project/bin; mkdir -p project/bin;\
	export JAVA_HOME=$(JAVA_HOME);\
	$(DX) --dex --output=$@ project/temp_output/classes
project/$(APK_NAME).apk: project/bin/pack-unsigned.apk project/mykey.keystore
	@$(ECHO) ${blue}Packaging APK${NC};\
	cp $< $@_;\
	$(ECHO) ${green}Signing APK${NC};\
	$(JARSIGNER) -verbose -storepass $(KEYSTORE_PASS) -signedjar $@ -sigalg SHA1withRSA -digestalg SHA1 -keystore project/mykey.keystore $@_ alias_name > /dev/null;\
	mv $@ $@_;\
	$(ECHO) ${green}Zipaligning APK${NC};\
	$(ZIPALIGN) -v 4 $@_ $@ > /dev/null;\
	rm -f $@_
	@$(PRINT_TIME_ELAPSED)

project/temp_output/classes_output/%.class:project/src/%.java
	@$(ECHO) ${green}Compiling $*.java${NC};
	@mkdir -p project/temp_output/classes;\
	mkdir -p project/temp_output/classes_output;\
	$(JDK_BIN)/javac -d $(NATIVE_PWD)/project/temp_output/classes -source 1.7 -target 1.7 -bootclasspath $(JAVA_HOME)/jre/lib/rt.jar -classpath "$(ANDROID_SDK)/platforms/android-$(TARGET_PLATFORM)/android.jar$(JAVA_SEPARATOR)$(NATIVE_PWD)/project/temp_output/classes" -sourcepath project/src $^;\
	touch $@

project/temp_output/classes/$(PACKAGE_DIR)/R.class:project/temp_output/R.java
	@$(ECHO)  ${blue}Creating R.java${NC}
	@mkdir -p project/temp_output/classes;\
	$(JDK_BIN)/javac -d project/temp_output/classes -bootclasspath $(JAVA_HOME)/jre/lib/rt.jar -source 1.7 -target 1.7 -classpath $(ANDROID_SDK)/platforms/android-$(TARGET_PLATFORM)/android.jar:$(NATIVE_PWD)/project/temp_output/classes -sourcepath project/src $^

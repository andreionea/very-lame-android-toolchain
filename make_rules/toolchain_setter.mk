PACKAGE_DIR:=$(shell echo "$(PACKAGE_NAME)" | sed -e "s/\./\//g")
ifeq ($(COMPILER),gcc)
	ifeq ($(DISTRIBUTED_BUILD),true)
		CXX:=distcc android-c++
		CC:=distcc android-cc
	else
		CXX:=android-c++
		CC:=android-cc
	endif
		LD:=android-c++
else
	CXX:=toolchain/bin/clang++ -target armv7-none-linux-androideabi 
	CC:=toolchain/bin/clang -target armv7-none-linux-androideabi
	LD:=toolchain/bin/clang++ -target armv7-none-linux-androideabi
endif

BINDIR:=$(PWD)/toolchain/bin/arm-linux-androideabi-
AR:=$(BINDIR)ar
OBJCOPY:=$(BINDIR)objcopy

CXXFLAGS+= -fpic -fno-builtin -marm -ffast-math -march=armv7-a -mfloat-abi=softfp -mfpu=vfpv3-d16
CXXFLAGS+= -fomit-frame-pointer
LDFLAGS:= --sysroot=toolchain/sysroot -Ltoolchain/lib -Ltoolchain/arm-linux-androideabi/lib -Llibs/
LDLIBS:= -lc -llog -landroid

ifeq ($(COMPILER),llvm)
	LDFLAGS+= -static-libstdc++ -Wl,--fix-cortex-a8
	LDLIBS+= -lc++_static -lc++abi -lunwind -landroid_support -lgcc
else
	LDLIBS+= -lstdc++ -lsupc++
endif

CXXFLAGS+= -march=armv7-a -mfloat-abi=softfp -mfpu=neon
CXXFLAGS+= -Wall -Wextra -Wno-unused-parameter -Wno-ignored-qualifiers -Wno-unused-but-set-variable -Wno-switch -Wno-unused-variable -Wno-unused-function
CXXFLAGS+= -w -O0 -g2 -gdwarf-2
CFLAGS:=$(CXXFLAGS)
CXXFLAGS+= -std=c++1z -Wno-reorder

# use for debugging using Tegra tools
# LDLIBS+= -lTegra_gfx_debugger -fno-lto
LDLIBS+= -lEGL -lGLESv3

LDLIBS+= -Wl,--whole-archive -lm $(addprefix -l, $(EXTERNAL_LIBS)) -Wl,--no-whole-archive

CCACHE:=ccache
ifeq ($(COMPILER), llvm)
toolchain:
	@python $(NDK_ROOT)/build/tools/make_standalone_toolchain.py --arch arm --stl=libc++ --api $(TARGET_PLATFORM) --install-dir $(PWD)/toolchain;\
	rm -f /usr/local/bin/android-*;\
	ln -s $(PWD)/toolchain/bin/arm-linux-androideabi-clang /usr/local/bin/android-cc;\
	ln -s $(PWD)/toolchain/bin/arm-linux-androideabi-clang++ /usr/local/bin/android-c++;
else
toolchain:
	@python $(NDK_ROOT)/build/tools/make_standalone_toolchain.py --arch arm --stl=gnustl --api $(TARGET_PLATFORM) --install-dir $(PWD)/toolchain;\
	rm -f /usr/local/bin/android-*;\
	ln -s $(PWD)/toolchain/bin/arm-linux-androideabi-gcc /usr/local/bin/android-cc;\
	ln -s $(PWD)/toolchain/bin/arm-linux-androideabi-g++ /usr/local/bin/android-c++;
endif
$(LIBDIR)/stripped_lib$(OUTPUT_LIB).so: $(LIBDIR)/lib$(OUTPUT_LIB).so
	@$(ECHO) Stripping $<
	@$(OBJCOPY) -S $< $@
	@$(PRINT_TIME_ELAPSED)

ifeq ($(COMPILER), llvm)
$(LIBDIR)/lib$(OUTPUT_LIB).so:$(addprefix $(LIBDIR)/lib,$(addsuffix .a, $(EXTERNAL_LIBS)))|$(PATCHES_DEST)
	@$(ECHO) ${purple}Linking $@${NC}
	@$(LD) -Wl,-soname,lib$(OUTPUT_LIB).so -shared -Wl,-undefined,error $(LDFLAGS) $(LDLIBS) -no-canonical-prefixes -Wl,--build-id -Wl,--no-undefined -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now -Wl,--warn-shared-textrel -Wl,--fatal-warnings -o $@
	@$(PRINT_TIME_ELAPSED)
else
$(LIBDIR)/lib$(OUTPUT_LIB).so:$(addprefix lib,$(addsuffix .a, $(EXTERNAL_LIBS)))|$(PATCHES_DEST)
	@$(ECHO) ${purple}Linking $@${NC}
	@$(LD) -shared -Wl,--no-undefined -Wl,-z,noexecstack -o $@ $(LDFLAGS) $(LDLIBS)
	@$(PRINT_TIME_ELAPSED)
endif
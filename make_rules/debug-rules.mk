###############################################################################
######################         Debugging rules           ######################
###############################################################################
libs:
	$(ADB) pull /system/lib libs/ ;\
	$(ADB) pull /system/vendor/lib libs/ ;\
	$(ADB) pull /system/vendor/lib/egl libs/ ;\
	$(ADB) pull /system/vendor/lib/hw libs/

debug: DATA_DIR=$(shell $(ADB) shell run-as $(PACKAGE_NAME) /system/bin/sh -c pwd)
debug: DEVICE_GDBSERVER=$(DATA_DIR)/lib/gdbserver
debug: PID=$(shell $(ADB) shell ps | grep $(PACKAGE_NAME) | tr -s ' ' | cut -d ' ' -f 2)
run-debug: install
	@$(ADB) shell am start -n $(PACKAGE_NAME)/android.app.NativeActivity
stop-debug: GDBSERVER_PID=$(shell $(ADB) shell ps | grep $(PACKAGE_NAME)\$ | tr -s ' ' | cut -d ' ' -f 2)
stop-debug: stop
	@$(ADB) shell run-as $(PACKAGE_NAME) kill -9 $(GDBSERVER_PID)
debug: run-debug libs
	$(ADB) shell run-as $(PACKAGE_NAME) $(DEVICE_GDBSERVER) +debug-socket --attach $(PID)& \
	$(ADB) forward tcp:$(GDB_DEBUG_PORT) localfilesystem:$(DATA_DIR)/debug-socket ; \
	touch gdb.setup ;\
	cat /dev/null > gdb.setup ;\
	$(ADB) pull /system/bin/app_process32 app_process ;\
	$(ADB) pull /system/bin/linker libs/;\
	cp lib$(OUTPUT_LIB).so libs/; \
	echo set solib-search-path $(PWD)/libs >> gdb.setup ;\
	echo file $(PWD)/app_process >> gdb.setup;\
	echo target remote localhost:$(GDB_DEBUG_PORT) >> gdb.setup
	# $(GDB) --interpreter=mi -x gdb.setup

###############################################################################
###############################################################################
###############################################################################

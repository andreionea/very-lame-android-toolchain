#requires LIBNAME SRCS NUM_CU
#exclude EXCLUDES
$(LIBNAME)_NCU:=$(NUM_CU)

#separate srcs for compilation units
ifneq ($(NUM_CU), 0)
ifneq ($(words $(EXCLUDES)), 0)
$(LIBNAME)_SRCS_FOR_CU:=$(filter-out $(EXCLUDES), $(SRCS))
else
$(LIBNAME)_SRCS_FOR_CU:=$(SRCS)
endif
$(LIBNAME)_CU_SRCS:=$(foreach x,$(shell seq 0 $(shell expr $(NUM_CU) - 1)), objs/$(SUBFOLDER)$(LIBNAME)_cu$(x).cpp)
$(LIBNAME)_CU_OBJS:=$(patsubst %.cpp, %.o, $($(LIBNAME)_CU_SRCS))
else
$(LIBNAME)_SRCS_FOR_CU:=
$(LIBNAME)_CU_OBJS:=
endif

$(LIBNAME)_INCLUDE_DIRS:=$(addprefix -I, $(INCLUDE_DIRS))
EXTRA_CXXFLAGS+=$($(LIBNAME)_INCLUDE_DIRS)
EXTRA_CFLAGS+=$($(LIBNAME)_INCLUDE_DIRS)

$(LIBNAME)_EXTRA_CXXFLAGS:=$(EXTRA_CXXFLAGS)
$(LIBNAME)_EXTRA_CFLAGS:=$(EXTRA_CFLAGS)
$(LIBNAME)_EXTRA_DEP:=$(EXTRA_DEP)

ifeq ($(ENABLE_PCH),1)
ifneq ($(PCH),none)
$(LIBNAME)_PCH_CXXFLAGS:=$(EXTRA_CXXFLAGS) $(CXXFLAGS)
ifeq ($(COMPILER), llvm)
$(LIBNAME)_EXTRA_CXXFLAGS:=$(EXTRA_CXXFLAGS) -include-pch $(PCH).gch
else
$(LIBNAME)_EXTRA_CXXFLAGS:=$(EXTRA_CXXFLAGS) -include $(PCH) -fpch-preprocess
endif
$(LIBNAME)_PCH_DEP:=$(EXTRA_DEP)
$(LIBNAME)_EXTRA_DEP:=$(EXTRA_DEP) $(PCH).gch
endif
endif

#separate srcs for individual compilation
ifneq ($(words $(EXCLUDES)), 0)
SRCS:=$(filter $(EXCLUDES), $(SRCS))
else
ifneq ($(NUM_CU), 0)
SRCS:=
endif
endif
$(LIBNAME)_OBJS:=$(patsubst %.cc, objs/%.o, $(SRCS))
$(LIBNAME)_OBJS:=$(patsubst %.cpp, objs/%.o, $($(LIBNAME)_OBJS))
$(LIBNAME)_OBJS:=$(patsubst %.c, objs/%.o, $($(LIBNAME)_OBJS))

DEP_FILES+=$(patsubst $(OBJDIR)/%.o, $(DEPDIR)/%.d, $($(LIBNAME)_OBJS))

$(LIBNAME)_SRCS:=$(SRCS) $($(LIBNAME)_SRCS_FOR_CU)
$(LIBNAME)_SUBFOLDER:=$(SUBFOLDER)

$(LIBNAME)_TARGET_DEPS:=$(TARGET_DEPS)

ALL_DEPS+=$($(LIBNAME)_OBJS) $($(LIBNAME)_EXTRA_DEP) $($(LIBNAME)_CU_OBJS)

include make_rules/clean_vars.mk
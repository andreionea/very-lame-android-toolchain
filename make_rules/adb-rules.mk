start:
	@bash -C ./scripts/unlock.sh; \
	$(ADB) shell am start -n $(PACKAGE_NAME)/android.app.NativeActivity
run: install unlock
	@$(ADB) shell am start -n $(PACKAGE_NAME)/android.app.NativeActivity;\
	$(ADB) forward tcp:$(DEBUG_PORT) tcp:$(DEBUG_PORT);\
	# $(OPEN) http://localhost:$(DEBUG_PORT)/upload.html
	@$(PRINT_TIME_ELAPSED)

stop: lock
	@$(ADB) shell am force-stop $(PACKAGE_NAME)

install: build
	@$(ADB) install -r project/$(APK_NAME).apk
	@$(PRINT_TIME_ELAPSED)
restart: stop
	@$(ADB) shell am start -n $(PACKAGE_NAME)/android.app.NativeActivity

lock:
	@./scripts/lock.sh &> /dev/null
unlock:
	@./scripts/unlock.sh &> /dev/null
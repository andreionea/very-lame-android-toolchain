#!/bin/bash
. paths.mk
ADB=$ANDROID_SDK/platform-tools/adb

if [ "$($ADB shell dumpsys power | grep mScreenOn= | grep -oE '(true|false)')" == true ] ; then
    $ADB shell input keyevent 26 # wakeup
elif [ "$($ADB shell dumpsys power | grep ^Display | grep -oE '(ON|OFF)')" == ON ] ; then
    $ADB shell input keyevent 26 # wakeup
fi

#!/bin/bash
. paths.mk
ADB=$ANDROID_SDK/platform-tools/adb

if [ "$($ADB shell dumpsys power | grep mScreenOn= | grep -oE '(true|false)')" == false ] ; then
    $ADB shell input keyevent 26 # wakeup
    $ADB shell input keyevent 82 # unlock
elif [ "$($ADB shell dumpsys power | grep ^Display | grep -oE '(ON|OFF)')" == OFF ] ; then
    $ADB shell input keyevent 26 # wakeup
    $ADB shell input keyevent 82 # unlock
fi

#!/bin/bash
END=`date +%s`
START=$1
DIFF=`expr $END - $START`
DIFFDAYS=`expr $DIFF / 86400`
REMSEC1=`expr $DIFF - 86400 \* ${DIFFDAYS}`
DIFFHOURS=`expr $REMSEC1 / 3600`
REMSEC2=`expr $DIFF - 3600 \* $DIFFHOURS`
DIFFMINS=`expr $REMSEC2 / 60`
DIFFSEC=`expr $DIFF - 60 \* $DIFFMINS`

black='\033[0;30m'
dark_gray='\033[1;30m'
blue='\033[0;34m'
light_blue='\033[1;34m'
green='\033[0;32m'
light_green='\033[1;32m'
cyan='\033[0;36m'
light_cyan='\033[1;36m'
red='\033[0;31m'
light_red='\033[1;31m'
purple='\033[0;35m'
light_purple='\033[1;35m'
orange='\033[0;33m'
yellow='\033[1;33m'
light_gray='\033[0;37m'
white='\033[1;37m'
NC='\033[0m'

echo -e $orange
printf "Elapsed time: %dd %02dh %02dm %02ds \n" $DIFFDAYS $DIFFHOURS $DIFFMINS $DIFFSEC
echo -e $NC
#!/usr/bin/python
import sys
import os

if __name__ == "__main__":
	library = sys.argv[1]
	subfolder = sys.argv[2]
	filename = os.path.join("prerequisites", library + "_cu.prereq")
	command = "cat make_rules/make_cu_rules.mk"
	subfolder_escaped = subfolder.replace('/', '\\\\/')
	command +=" | sed s/\$\(LIBNAME\)/" + library + "/g"
	command +=" | sed s/\$\(SUBFOLDER\)/" + subfolder_escaped + "/g"
	command +=" >> " + filename
	if len(sys.argv) > 3:
		print library
		num_cu = int(sys.argv[3])
		srcs = sys.argv[4].split(' ')
		if num_cu != 0:
			srcs_per_cu = len(srcs) / num_cu
		else:
			srcs_per_cu = 0
		with open(filename , "a") as depfile:
			for i in range(num_cu):
				l = srcs[i * srcs_per_cu: (i+1) * srcs_per_cu]
				if i == (num_cu - 1):
					l = srcs[i * srcs_per_cu:]
				is_c = len(l) > 0 and l[0].endswith('.c')
				if is_c:
					extension = '.c'
				else:
					extension = '.cpp'
				depfile.write("$(OBJDIR)/"+subfolder+library+"_cu"+str(i)+ extension + ": ")
				for j in l:
					depfile.write(j + " ")
				depfile.write("\n")
				depfile.write("\t@mkdir -p $(dir $@)\n")
				depfile.write("\t@cat /dev/null > $@\n")


				if sys.platform == "cygwin":
					depfile.write("\t@echo -e $(foreach x,$^,\\#include \\\"$x\\\"\'\\n\')>> $@")
				else:
					depfile.write("\t@echo $(foreach x,$^,\\#include \\\"$x\\\"\'\\n\')>> $@")

				depfile.write("\n\n\n")

	os.system(command)

#ifndef JNIUtils_h
#define JNIUtils_h

#include "Utils.h"
#include <jni.h>
#include <string>

class JavaClass;

class ScopedJNI
{
public:
	ScopedJNI() {
		auto utils = Utils::GetInstance();
		utils->m_envMtx.lock();
		utils->m_nativeapp->vm->AttachCurrentThread( &env, NULL );
	}
	~ScopedJNI()
	{
		auto utils = Utils::GetInstance();
		utils->m_nativeapp->vm->DetachCurrentThread();
		utils->m_envMtx.unlock();
	}

	static JNIEnv* env;
};

class JavaClass {

private:	

	
	jmethodID GetInstanceMethodID(const std::string& name, const std::string& sig)
	{
		// This function should never be called except at init
		JNIEnv* env = ScopedJNI::env;
		return env->GetMethodID(m_classID, name.c_str(), sig.c_str());
	}

	jmethodID GetStaticMethodID(const std::string& name, const std::string& sig)
	{
		// This function should never be called except at init
		JNIEnv* env = ScopedJNI::env;
		return env->GetStaticMethodID(m_classID, name.c_str(), sig.c_str());
	}

	void CacheStaticMethod(const std::string& name, const std::string& sig)
	{
		m_staticMets[name + "|" + sig] = GetStaticMethodID(name, sig);
	}

	void CacheInstanceMethod(const std::string& name, const std::string& sig)
	{
		m_instanceMets[name + "|" + sig] = GetStaticMethodID(name, sig);
	}



public:
	JavaClass(jclass cls) : m_classID(cls) {}
	JavaClass() {}
	JavaClass(const JavaClass& other) : m_classID(other.m_classID)
	{
		m_staticMets = other.m_staticMets;
		m_instanceMets = other.m_instanceMets;
	}

	static const JavaClass* GetClass(const std::string &name)
	{
		auto it = s_classes.find(name);
		if (it == s_classes.end())
			return nullptr;
		return &(it->second);
	}

	jmethodID GetInstanceMethodID(const std::string& name, const std::string& sig) const
	{
		auto it = m_instanceMets.find(name + "|" + sig);
		if (it != m_instanceMets.end())
			return it->second;
		return nullptr;	
	}
	jmethodID GetStaticMethodID(const std::string& name, const std::string& sig) const
	{
		auto it = m_staticMets.find(name + "|" + sig);
		if (it != m_staticMets.end())
			return it->second;
		return nullptr;
	}

	void RunTest() 
	{
		// jmethodID id = GetStaticMethodID("TestFct", "()V");
	 	// env->CallStaticVoidMethod(m_classID, id, NULL);
	}

private:

	static void CacheClass(const std::string& name, JavaClass& cls)
	{
		s_classes[name] = cls;
	}

	std::map<std::string, jmethodID> m_staticMets;
	std::map<std::string, jmethodID> m_instanceMets;
	jclass m_classID;
	static std::map<std::string, JavaClass> s_classes;
	friend class Utils;
};
#endif
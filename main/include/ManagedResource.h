#ifndef ManagedResource_h
#define ManagedResource_h

#include <memory>
#include <map>
#include <string>

template<class Resource>
class ManagedResource {
	public:
		static std::shared_ptr<Resource> GetResource(const std::string& key) {
			auto end = s_resources.end();
			auto x = s_resources.find(key);
			if (x == end || x->second.expired()) 
			{
				auto revive = s_purgatory.find(key);
				if (revive != s_purgatory.end()) {
					auto ptr = revive->second;
					s_purgatory.erase(revive);
					auto ret = std::shared_ptr<Resource>(ptr, std::bind(&RemoveResource, std::placeholders::_1, key));
					std::weak_ptr<Resource> weak = ret;
					s_resources[key] = weak;
					return ret;
				}
				auto ret = std::shared_ptr<Resource>(new Resource(key), std::bind(&RemoveResource, std::placeholders::_1, key));
				std::weak_ptr<Resource> weak = ret;
				s_resources[key] = weak;
				return ret;
			} 
			return x->second.lock();

		}
		static void PurgeResources() {
			for (auto &i : s_purgatory)
				delete i.second;
			s_purgatory.clear();
		}
		virtual ~ManagedResource() {}
	private:
		static void RemoveResource(Resource* res, const std::string key)
		{
			s_purgatory[key] = res;
		}
		static std::map<std::string, std::weak_ptr<Resource>> s_resources;
		static std::map<std::string, Resource*> s_purgatory;
};

#define DECL_MANAGED_RES(classname) \
private:\
	classname(const std::string&);\
	virtual ~classname();\
	classname(const classname &) = delete;\
	classname& operator=(const classname&) = delete;\
	friend class ManagedResource<classname>

template<typename T>
std::map<std::string, std::weak_ptr<T>> ManagedResource<T>::s_resources;
template<typename T>
std::map<std::string, T*> ManagedResource<T>::s_purgatory;
#endif
#pragma once

#include <GLES3/gl3.h>
#include <string>

#include "ManagedResource.h"

class ShaderProgram: public ManagedResource<ShaderProgram>
{
	DECL_MANAGED_RES(ShaderProgram);
public:
	bool Initialize();
	void Bind();
private:
	GLuint m_prog;
	std::string m_filename;
	std::string m_name;
	bool m_initialized;
};

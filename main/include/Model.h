#ifndef Model_h
#define Model_h 

#include "ManagedResource.h"
#include <string>
#include <assimp/Importer.hpp>

class Model : public ManagedResource<Model>
{
DECL_MANAGED_RES(Model);
	std::string m_filename;
	Assimp::Importer m_importer;
public:
	void Initialize();
};

#endif
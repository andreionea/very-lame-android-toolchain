#ifndef HTTPDebugger_h
#define HTTPDebugger_h

#include "Singleton.h"
#include <string>
#include <microhttpd.h>

class HTTPDebugger: public Singleton<HTTPDebugger>
{
DECLARE_SINGLETON(HTTPDebugger);
	int m_port;
	MHD_Daemon *m_daemon;
	struct Req_POST
	{
		std::string url;
		char* data;
		size_t size;
		size_t capacity;
		Req_POST() : size(0), capacity(100), data(new char[100]) {}
		~Req_POST() {delete[] data;}
	};
	static int
	http_ahc (void *cls,
	          struct MHD_Connection *connection,
	          const char *url,
	          const char *method,
	          const char *version,
	          const char *upload_data,
		  size_t *upload_data_size, void **ptr);
	static void request_completed(void *cls, struct MHD_Connection * connection, void **con_cls, enum MHD_RequestTerminationCode toe);
public:
	void SetPort(int port) { m_port = port;}
	void Pause();
	void Resume();
};
#endif
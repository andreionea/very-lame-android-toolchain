#ifndef Texture_h
#define Texture_h

#include "ManagedResource.h"
#include "FileRes.h"
class Texture: public ManagedResource<Texture>
{
	DECL_MANAGED_RES(Texture);
public:
	enum ColorFormat {
		RGB888,
		RGBA8888
	};
	// TODO: make it private
	void Initialize();
	void Bind();
	void Update(const char*, size_t);
private:
	void Load(const char*, size_t);
	const char* m_swappedData;
	size_t m_swappedSize;
	std::shared_ptr<FileRes> m_file;
	unsigned m_handle;
	unsigned m_width, m_height;
	bool m_initialized;
	ColorFormat m_format;
	bool m_swapped;
};
#endif
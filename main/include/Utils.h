#ifndef Utils_h
#define Utils_h

#include <memory>
#include <mutex>
#include <jni.h>
#include <map>
#include "Singleton.h"
#include <android/native_activity.h>
#include <functional>

class ScopedJNI;
class JavaClass;

class Utils: public Singleton<Utils>
{
DECLARE_SINGLETON(Utils);
public:
	void SetNativeActivity(struct ANativeActivity* activity) { m_nativeapp = activity; }
	struct ANativeActivity* GetNativeActivity() const {return m_nativeapp; }
	void LoadClasses();
	void EnableImmersive();
	void PrintToast(const std::string&);
private:
	friend class ScopedJNI;
	friend class JavaClass;
	struct ANativeActivity* m_nativeapp;
	std::mutex m_envMtx;
};

#endif
#pragma once

#include "Singleton.h"
#include "EGLWrapper.h"
#include "android_native_app_glue.h"
#include <sol.hpp>
class TestActivity : public Singleton<TestActivity>
{
DECLARE_SINGLETON(TestActivity);
public:
	virtual void MainLoop();
	virtual void Update();
	virtual void Draw();
	void Initialize();
	void InitializeEGL(ANativeWindow*);
	virtual void LoopEvents();
	virtual void OnPause();
	void DestroyWindow();
	void SetApp(android_app* app) { m_app = app; }
	void InitLua();
	bool HasContext();
private:
	struct android_app* m_app;
	EGLWrapper m_egl;
	bool m_init;
	bool m_luaInit;
	sol::state m_luaState;
};

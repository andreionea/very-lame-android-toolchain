#include "config.h"
#include "Texture.h"

#include "Utils.h"

#include <sstream>
#include <iomanip>
#include <zlib.h>
#include <json/json.h>
#include "unzip.h"
#include "ioapi_mem.h"
#include "png.h"
#include <cstring>
#include <GLES3/gl3.h>
#include <jpeglib.h>

Texture::~Texture()
{
	if (m_handle)
		glDeleteTextures(1, &m_handle);
}

Texture::Texture(const std::string& filename): m_width(0), m_height(0), m_initialized(false), m_swapped(false), m_format(RGB888)
{
	m_file = FileRes::GetResource(filename);
}


// Private class now, might move it later
struct ScopedZip
{
	unzFile m_file;
	size_t m_size;
	bool m_opened;
	bool m_valid;
	char* m_data; // This data is NOT deleted by ScopedZip

	ScopedZip(const char* filename, unzFile file): m_file(file), m_opened(false), m_valid(false), m_size(0), m_data(0)
	{
		auto x = unzLocateFile(m_file, filename, NULL);
		if (UNZ_OK == x ){
			unz_file_info file_info;
			unzGetCurrentFileInfo(m_file, &file_info, NULL, 0, NULL, 0, NULL, 0);	
			m_size = file_info.uncompressed_size;
			auto y = unzOpenCurrentFile(m_file);
			m_opened = true;
			if (UNZ_CRCERROR != y)
				m_valid = true;
			else
				LOGME("Invalid %s", filename);
		}
		else {
			LOGME("Could not locate %s", filename);
		}
	}

	char* Data()
	{
		if (!m_opened || !m_valid)
			return NULL;
		if (m_data)
			return m_data;
		m_data = new char[m_size + 1];
		unzReadCurrentFile(m_file, m_data, m_size);
		return m_data;
	}

	size_t Size()
	{
		return m_size;
	}

	~ScopedZip()
	{
		if (m_opened)
			unzCloseCurrentFile(m_file);
	}
	ScopedZip(const ScopedZip&) = delete;
};

struct ScopedPNG
{
	const char* m_data;
	char* m_decompressed;
	size_t m_cursor;
	size_t m_size;
	png_uint_32 m_width, m_height;
	int m_depth;
	int m_colortype;
	ScopedPNG(const char* data, size_t size):m_data(data), m_cursor(0), m_size(size), m_decompressed(0)
	{
	}
	char* readPNG()
	{
		if (m_decompressed)
			return m_decompressed;
		auto png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL, NULL, NULL);
		if (!png_ptr)
			return NULL;
		auto info_ptr = png_create_info_struct(png_ptr);
		if (!info_ptr) {
			png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
			return NULL;
		}
		png_set_read_fn(png_ptr, this, ScopedPNG::read);
		png_read_png(png_ptr, info_ptr, 0, NULL);
		png_get_IHDR(png_ptr, info_ptr, &m_width, &m_height, &m_depth, &m_colortype, NULL, NULL, NULL);
		png_bytep *row_pointers = png_get_rows(png_ptr, info_ptr);
		size_t bpr = png_get_rowbytes(png_ptr, info_ptr);
		size_t aligned4 = ((bpr / 4) + (((bpr % 4) > 0) ? 1 : 0) ) * 4;
		m_decompressed = new char[aligned4 * m_height];
		char* ptr = m_decompressed;
		for (decltype(m_height) i = m_height; i > 0; --i, ptr+=aligned4) {
			memcpy(ptr, row_pointers[i - 1], bpr);
		}
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		LOGME("W:%d H:%d", m_width, m_height);
		LOGME("Size: %f", bpr / 4.0f);
		LOGME("Finished decompressing");
		return m_decompressed;		
	}
	static void read(png_structp png, png_bytep data, png_size_t toread)
	{
		ScopedPNG* spng = reinterpret_cast<ScopedPNG*>(png_get_io_ptr(png));
		memcpy(data, spng->m_data + spng->m_cursor, toread);
		spng->m_cursor += toread;
	}
	~ScopedPNG()
	{
		if (m_decompressed)
			delete[] m_decompressed;
		if (m_data)
			delete[] m_data;
	}
};

struct ScopedJPEG
{
	const char* m_data;
	char* m_decompressed;
	size_t m_size;
	uint32_t m_width, m_height;
	ScopedJPEG(const char* data, size_t size):m_data(data), m_size(size), m_decompressed(0)
	{
	}
	char* readJPEG()
	{
		struct jpeg_decompress_struct cinfo; 
		jpeg_create_decompress(&cinfo);
		struct jpeg_error_mgr jerr;		
		cinfo.err = jpeg_std_error(&jerr);
		jpeg_mem_src(&cinfo, (unsigned char*)(m_data), m_size);
		jpeg_read_header(&cinfo, TRUE);
		jpeg_start_decompress(&cinfo);
		size_t decompressedSize;
		m_width = cinfo.output_width;
		m_height = cinfo.output_height;
		LOGME("JPEG SIZE %d %d %d", m_width, m_height, cinfo.output_components);
		size_t rowSize = m_width * cinfo.output_components;
		rowSize += (4 - rowSize % 4) % 4;
		m_decompressed = new char[rowSize * m_height];
		while (cinfo.output_scanline < cinfo.output_height) {
			unsigned char* line = (unsigned char*)(m_decompressed + (m_height - cinfo.output_scanline - 1) * rowSize);
			auto x = jpeg_read_scanlines(&cinfo, &line, 1);
		}
		jpeg_finish_decompress(&cinfo);
		jpeg_destroy_decompress(&cinfo);
		return m_decompressed;
	}
	~ScopedJPEG() {
		if (m_data)
			delete[] m_data;
		if (m_decompressed)
			delete[] m_decompressed;
	}
};


static bool isPNG(const char* data, size_t size)
{
	if (size < 8)
		return false;
	const int HEADER_SIZE = 8;
	static const char header[HEADER_SIZE] = {0x89, 0x50, 0x4e, 0x47, 0xd, 0xa, 0x1a, 0xa};
	for (int i = 0; i < HEADER_SIZE; ++i)
		if (data[i] != header[i])
			return false;
	return true;
}
static bool isJFIF(const char* data, size_t size)
{
	static const char HEADER[5] = {'J', 'F', 'I', 'F', 0};
	for (int i = 0; i < 5; ++i)
		if (data[i] != HEADER[i])
			return false;
	return true;
}
static bool isEXIF(const char* data, size_t size)
{
	static const char HEADER[5] = {'E', 'X', 'I', 'F', 0};
	for (int i = 0; i < 5; ++i)
		if (toupper(data[i]) != HEADER[i])
			return false;
	return true;
}
static bool isJPEG(const char* data, size_t size)
{
	if (size < 9)
		return false;
	static const char APP0[3] = { 0xFF, 0xd8,0xff};
	for (int i = 0; i < 3; ++i)
		if (data[i] != APP0[i]){
			Utils::GetInstance()->PrintToast("Different i");
			return false;
		}
	if (data[3] != 0xe0 && data[3] != 0xe1){
		Utils::GetInstance()->PrintToast("Different last");
		return false;
	}

	return isJFIF(data + 6, size) || isEXIF(data + 6, size);
}
void Texture::Load(const char* data, size_t size)
{
	if (isPNG(data, size)){
		ScopedPNG spng(data, size);
		char* pngData =	 spng.readPNG();
		m_width = spng.m_width;
		m_height = spng.m_height;
		switch(spng.m_colortype){
			case PNG_COLOR_TYPE_RGB:
				m_format = RGB888;
				LOGME("no alpha");
				break;
			case PNG_COLOR_TYPE_RGBA:
				m_format = RGBA8888;
				LOGME("alpha");
				break;
			default:
				LOGME("m_colortype %d", spng.m_colortype);
				m_format = RGB888;		
					
		}
		glBindTexture(GL_TEXTURE_2D, m_handle);
		// glTexStorage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height);
		if (m_format == RGB888){
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_width, m_height, GL_RGB, GL_UNSIGNED_BYTE, pngData);
		}
		else
			if (m_format == RGBA8888){
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
				glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_width, m_height, GL_RGBA, GL_UNSIGNED_BYTE, pngData);
			}
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	else
	{
		std::ostringstream oss;
		oss << std::setbase(16);
		oss << "IMG header:" << (int)data[0] << " " << (int)data[1] << " "<< (int)data[2] << " " << (int)data[3];
		Utils::GetInstance()->PrintToast(oss.str());
		if (isJPEG(data, size)) {
			ScopedJPEG sjpg(data, size);
			char* jpgData = sjpg.readJPEG();
			m_format = RGB888;
			m_width = sjpg.m_width;
			m_height = sjpg.m_height;
			glBindTexture(GL_TEXTURE_2D, m_handle);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_RGB, GL_UNSIGNED_BYTE, jpgData);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glBindTexture(GL_TEXTURE_2D, 0);
		}
		else{
			LOGME("unknown format");
			delete[] data;
		}
	}
}

void Texture::Update(const char* data, size_t size)
{
	LOGME("data %p size %d", data, size);
	m_swappedData = data;
	m_swappedSize = size;
	m_swapped = true;
}

void Texture::Initialize()
{
	zlib_filefunc_def filefunc32;
	ourmemory_t unzmem;
	glGenTextures(1, &m_handle);
	fill_memory_filefunc(&filefunc32, &unzmem);
	unzmem.base = const_cast<char*>(m_file->Data());
	unzmem.size = m_file->Size();

	unzFile f = unzOpen2("__notused__", &filefunc32);

	if(f) 
	{
		char *data;
		size_t size;
		{
			ScopedZip sz("info.json", f);
			data = sz.Data();
			size = sz.Size();
		}
		if (data) {
			LOGME("Read info.json, got this:\n%s", data);
			Json::Reader reader;
			Json::Value root;
			reader.parse(data, data + size, root);
			std::string format = root["file_format"].asString();
			if (format == "PNG") {
				LOGME("Is PNG");
				ScopedZip sz2(root["filename"].asCString(), f);
				Load(sz2.Data(), sz2.Size());
			}
		}
		else 
			LOGME("Could not read info.json");
		delete[] data;
	}
	if (f)
		unzClose(f);
	m_initialized = true;
}

void Texture::Bind()
{
	if (m_swapped) {
		Load(m_swappedData, m_swappedSize);
		m_swapped = false;
		Utils::GetInstance()->PrintToast("Swap");
	}
	glBindTexture(GL_TEXTURE_2D, m_handle);
}
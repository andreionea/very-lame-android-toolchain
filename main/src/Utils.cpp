#include "config.h"
#include "Utils.h"
#include "JNIUtils.h"
#include <json/json.h>
#include "FileRes.h"
DEFINE_SINGLETON(Utils);

JNIEnv* ScopedJNI::env = nullptr;
std::map<std::string, JavaClass> JavaClass::s_classes;

Utils::Utils(): m_nativeapp(0)
{

}

void Utils::LoadClasses()
{
	ScopedJNI jni;

	JNIEnv* env = ScopedJNI::env;

	jclass nativejclass = env->FindClass("android/app/NativeActivity");
	JavaClass nativecls = JavaClass(nativejclass);
	jmethodID getClassLoader = nativecls.GetInstanceMethodID("getClassLoader", "()Ljava/lang/ClassLoader;");

	jobject cls = env->CallObjectMethod(Utils::GetInstance()->m_nativeapp->clazz, getClassLoader);
	jclass classLoader = env->FindClass("java/lang/ClassLoader");
	jmethodID findClass = env->GetMethodID(classLoader, "loadClass", "(Ljava/lang/String;)Ljava/lang/Class;");

	auto classFile = FileRes::GetResource("asset://classes.json");

	Json::Reader reader;
	Json::Value root;
	auto data = classFile->Data();
	auto size = classFile->Size();
	reader.parse(data, data + size, root);

	auto classes = root["classes"];

	auto num_classes = classes.size();
	LOGME("%d classes", num_classes);

	for (int i = 0; i < num_classes; ++i)
	{
		auto classItem = classes[i];
		std::string name = classItem["name"].asString();
		LOGME("Cache class: %s", name.c_str());

		jstring strClassName = env->NewStringUTF(name.c_str());
		jclass classIWant = (jclass)env->CallObjectMethod(cls, findClass, strClassName);
		classIWant = (jclass)env->NewGlobalRef(classIWant);
		env->DeleteLocalRef(strClassName);

		JavaClass javaclass(classIWant);

		auto static_methods = classItem["static"];
		int num_static = static_methods.size();
		for (int j = 0; j < num_static; ++j)
		{
			auto met = static_methods[j];
			std::string met_name = met["name"].asString();
			std::string met_sig  = met["sig" ].asString();
			javaclass.CacheStaticMethod(met_name, met_sig);
			LOGME("%s: %s method cached", met_name.c_str(), met_sig.c_str());
		}

		auto instance_methods = classItem["instance"];
		int num_instance = instance_methods.size();
		for (int j = 0; j < num_instance; ++j)
		{
			auto met = instance_methods[j];
			std::string met_name = met["name"].asString();
			std::string met_sig  = met["sig" ].asString();
			javaclass.CacheInstanceMethod(met_name, met_sig);
		}

		JavaClass::CacheClass(name, javaclass);
	}
	//Set Context
	const JavaClass* clss = JavaClass::GetClass("com/andreionea/Utils");
	jmethodID id = clss->GetStaticMethodID("SetContext", "(Landroid/app/NativeActivity;)V");
	env->CallStaticVoidMethod(clss->m_classID, id, m_nativeapp->clazz);
}
void Utils::EnableImmersive()
{
	ScopedJNI jni;
	JNIEnv* env = ScopedJNI::env;
	const JavaClass* cls = JavaClass::GetClass("com/andreionea/Utils");
	jmethodID id = cls->GetStaticMethodID("EnableImmersive", "()V");
	env->CallStaticVoidMethod(cls->m_classID, id);
}
void Utils::PrintToast(const std::string& str)
{
	ScopedJNI jni;

	auto env = ScopedJNI::env;
	auto cls = JavaClass::GetClass("com/andreionea/Utils");
	auto id = cls->GetStaticMethodID("PrintToast", "(Ljava/lang/String;)V");
	auto javastr = env->NewStringUTF(str.c_str());
	env->CallStaticVoidMethod(cls->m_classID, id, javastr);
}

Utils::~Utils()
{

}
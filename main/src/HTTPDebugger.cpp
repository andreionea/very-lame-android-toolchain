#include "config.h"

#include "HTTPDebugger.h"
#include <microhttpd.h>
#include "FileRes.h"
#include <cstring>
#include "Texture.h"
#include <iomanip>

#include "Utils.h"
#include <sstream>

DEFINE_SINGLETON(HTTPDebugger);

HTTPDebugger::HTTPDebugger() : m_port(8085), m_daemon(NULL) {}
HTTPDebugger::~HTTPDebugger()
{
	Pause();
}

void HTTPDebugger::Pause()
{
	if (m_daemon) {
		MHD_stop_daemon (m_daemon);
		m_daemon = NULL;
	}
}

int
HTTPDebugger::http_ahc (void *cls,
          struct MHD_Connection *connection,
          const char *url,
          const char *method,
          const char *version,
          const char *upload_data,
	  size_t *upload_data_size, void **ptr)
{
	if (!*ptr)
	{
		LOGME("HTTP ME %s", url);
		struct MHD_Response *response;
		int ret;
		std::string str(url);
		if (0 == strncmp(method, "GET", 3)) {
			if (str.length() == 1) {
			  	const char *page  = "<html><body>Hello, browser!</body></html>";
				response = MHD_create_response_from_buffer (strlen(page),
									  (void*)page,
									  MHD_RESPMEM_MUST_COPY);

				ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
				MHD_destroy_response (response);
			}
			else{
				auto file = FileRes::GetResource(std::string("asset:/")+str);
				if (file->Valid()){
					auto page = file->Data();
					response = MHD_create_response_from_buffer (file->Size(),
										  (void*)page,
										  MHD_RESPMEM_MUST_COPY);

					ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
					MHD_destroy_response (response);
				}
				else {
					const char *page  = "<html><body>Not found!</body></html>";
					response = MHD_create_response_from_buffer (strlen(page),
										  (void*)page,
										  MHD_RESPMEM_MUST_COPY);

					ret = MHD_queue_response (connection, MHD_HTTP_NOT_FOUND, response);
					MHD_destroy_response (response);
				}
			}
			return ret;  	
		}
		else {
			Req_POST *req = new Req_POST();
			LOGME("data %p", req->data);
			req->url = url;
			*ptr = req;
			return MHD_YES;
		}
	}
	else {
		Req_POST *req = (Req_POST*)(*ptr);
		if (*upload_data_size == 0) {
			const char *page  = "Okie dokie";
			auto response = MHD_create_response_from_buffer (strlen(page),
								  (void*)page,
								  MHD_RESPMEM_MUST_COPY);

			MHD_queue_response (connection, MHD_HTTP_OK, response);
			MHD_destroy_response (response);
			*ptr = NULL;
			auto tex = Texture::GetResource("asset://archive.zip");
			std::ostringstream oss;
			oss << std::setbase(16);
			oss << "Request size:" << req->size << " " << (long)req->data;
			Utils::GetInstance()->PrintToast(oss.str());
			tex->Update(req->data, req->size);
			req->data = 0;
			delete req;
		}
		else {
			if (req->size + *upload_data_size >= req->capacity) {
				while (req->size + *upload_data_size >= req->capacity)
					req->capacity *= 2;
				char* newdata = new char[req->capacity];
				std::memcpy(newdata, req->data, req->size);
				delete[] req->data;
				req->data = newdata;
				LOGME("data resize %p", req->data);
			}
			std::memcpy(req->data + req->size, upload_data, *upload_data_size);
			req->size += *upload_data_size;
		}
		*upload_data_size = 0;
		return MHD_YES;
	}
}
void HTTPDebugger::Resume()
{
	if (!m_daemon)
	m_daemon = MHD_start_daemon (MHD_USE_SELECT_INTERNALLY	| MHD_USE_DEBUG,
                        m_port,
                        NULL, NULL, 
                        &http_ahc, NULL, 
                        MHD_OPTION_END);
}
#include "config.h"
#include "Model.h"
#include "FileRes.h"
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

Model::Model(const std::string& filename) : m_filename(filename)
{

}

Model::~Model()
{

}

void Model::Initialize()
{ 
	auto file = FileRes::GetResource(m_filename);
	m_importer.ReadFileFromMemory(file->Data(), file->Size(), aiProcess_JoinIdenticalVertices | aiProcess_OptimizeMeshes);
	if (m_importer.GetScene())
		LOGME("Loaded scene");
	else
		LOGME("Could not load scene");
}
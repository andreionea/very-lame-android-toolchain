#include "config.h"
#include "TestActivity.h"
#include <android/log.h>
#include <android/window.h>


#include "Utils.h"
#include "FileRes.h"
#include "ShaderProgram.h"
#include "Texture.h"
#include "Model.h"

#include <lua.hpp>
#include <sol.hpp>

#include "HTTPDebugger.h"
#include "JNIUtils.h"

std::shared_ptr<ShaderProgram> sp;
std::shared_ptr<Texture> tex;
std::shared_ptr<Model> model;

DEFINE_SINGLETON(TestActivity);

void onAppCmd(struct android_app* app, int32_t cmd);

void Print(const char* str)
{
	LOGME("Here's the output %s", str);
}

void TestActivity::Initialize()
{
	if (m_init)
	{
		Utils::GetInstance()->SetNativeActivity(m_app->activity);
		Utils::GetInstance()->EnableImmersive();
		return;
	}
	
	Utils::CreateInstance();
	Utils::GetInstance()->SetNativeActivity(m_app->activity);


	HTTPDebugger::CreateInstance();
	HTTPDebugger::GetInstance()->SetPort(8085);


	Utils::GetInstance()->LoadClasses();
	Utils::GetInstance()->EnableImmersive();

	m_init = true;
}
void android_main(struct android_app* app)
{
	app_dummy();
	TestActivity::CreateInstance();
	TestActivity* activity = TestActivity::GetInstance();


	activity->SetApp(app);

	activity->Initialize();

	ANativeActivity_setWindowFlags(app->activity, AWINDOW_FLAG_KEEP_SCREEN_ON | AWINDOW_FLAG_FULLSCREEN, 0);
	app->userData = activity;
	app->onAppCmd = onAppCmd;
	activity->MainLoop();

	app->userData = 0;
	TestActivity::DestroyInstance();
}

void TestActivity::LoopEvents()
{
	int32_t ident;
	int32_t events;
	struct android_poll_source* source;

	while ((ident = ALooper_pollAll(1, NULL, &events, (void**)&source)) >= 0)
	{
		if (source != NULL)
		{
			source->process(m_app, source);
		}
    }

}

void TestActivity::Update()
{
	// This will be done only once
	InitLua();
}

void TestActivity::Draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glViewport(0, 0, m_egl.m_width, m_egl.m_height);
	sp->Bind();
	glActiveTexture(GL_TEXTURE0);
	glUniform1i(0, 0);
	tex->Bind();
	static const float positions[8] =
	{
		-1.0,  1.0,
		 1.0,  1.0,
		 1.0, -1.0,
		-1.0, -1.0
	};
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, &positions[0]);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	m_egl.SwapBuffers();
}
void TestActivity::MainLoop()
{
	while (true) {
		LoopEvents();
		if (HasContext()) {
			Update();
			Draw();
		}
	}
}

bool TestActivity::HasContext()
{
	return m_egl.isInitialized();
}

void TestActivity::InitLua()
{
	if (m_luaInit)
		return;
	auto file = FileRes::GetResource("asset://script.lua");
	m_luaState["alog"] = &Print;
	m_luaState.script(file->Data());
	m_luaInit = true;
}

void TestActivity::InitializeEGL(ANativeWindow* window)
{
	m_egl.Initialize(window);
	if (!sp)
		sp = ShaderProgram::GetResource("asset://simple_tex.prog");
	bool result = sp->Initialize();
	if (!result)
		LOGME("WTF?!");
	if (!tex){
		tex = Texture::GetResource("asset://archive.zip");
		tex->Initialize();
	}
	if (!model){
		model = Model::GetResource("asset://bunny.dae");
		model->Initialize();
	}

}

TestActivity::TestActivity():m_init(false), m_luaInit(false)
{

}

TestActivity::~TestActivity()
{

}

void TestActivity::OnPause()
{
}

void TestActivity::DestroyWindow()
{
	m_egl.DestroySurface();
}
void onAppCmd(struct android_app* app, int32_t cmd)
{
	TestActivity* activity = (TestActivity*)(app->userData);
	switch(cmd) {
		case APP_CMD_INIT_WINDOW:
			LOGME("Init window");
			Utils::GetInstance()->PrintToast("Init window");
			activity->InitializeEGL(app->window);
			HTTPDebugger::GetInstance()->Resume();
			break;
		case APP_CMD_TERM_WINDOW:
			activity->DestroyWindow();
			break;
		case APP_CMD_CONTENT_RECT_CHANGED:
			LOGME("Rect changed");
			Utils::GetInstance()->PrintToast("Rect changed");
			break;
		case APP_CMD_WINDOW_RESIZED:
			LOGME("Resize");
			Utils::GetInstance()->PrintToast("Resize");
			break;
		case APP_CMD_GAINED_FOCUS:
			LOGME("Focus");
			Utils::GetInstance()->PrintToast("Focus");
			Utils::GetInstance()->EnableImmersive();
			break;
		case APP_CMD_RESUME:
			Utils::GetInstance()->PrintToast("Resume");
			Utils::GetInstance()->EnableImmersive();
			break;
		case APP_CMD_PAUSE:
			Utils::GetInstance()->PrintToast("Pause");
			activity->OnPause();
			HTTPDebugger::GetInstance()->Pause();
			break;

	}
}
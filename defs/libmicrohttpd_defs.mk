LIBNAME:=libmicrohttpd
SRCS:=$(shell ls externals/libmicrohttpd/src/daemon/*.c | grep -v "_test.c" | grep -v "_https.c")
EXTRA_CFLAGS:= -DDAUTH_SUPPORT
INCLUDE_DIRS:=externals/libmicrohttpd/src/include/plibc externals/libmicrohttpd/symbian externals/libmicrohttpd/src/include
SUBFOLDER:=externals/
include make_rules/make_cu_defs.mk



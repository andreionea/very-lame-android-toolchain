LIBNAME:=main
SRCS:=$(shell ls main/src/*.cpp)
SRCS+=main/src/android_native_app_glue.c
INCLUDE_DIRS:=main/include .
EXTRA_DEP:=externals/libjpeg/jconfig.h externals/jsoncpp/dist/json/json.h main/src/android_native_app_glue.c main/include/android_native_app_glue.h
EXTRA_CXXFLAGS:=-Wno-register
TARGET_DEPS:=assimp jsoncpp libjpeg libmicrohttpd libpng lua sol2 minizip zlib
PCH:=main/include/MainPCH.h
# NUM_CU:=1
# EXCLUDES:=%.c
include make_rules/make_cu_defs.mk
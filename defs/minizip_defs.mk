LIBNAME:=minizip
SRCS:=externals/minizip/unzip.c externals/minizip/zip.c externals/minizip/ioapi.c externals/minizip/ioapi_mem.c
EXTRA_CFLAGS:=-DUSE_FILE32API
INCLUDE_DIRS:=externals/minizip
SUBFOLDER:=externals/
include make_rules/make_cu_defs.mk


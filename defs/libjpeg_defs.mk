LIBNAME:=libjpeg
SRCS:=externals/libjpeg/jaricom.c \
			externals/libjpeg/jcapimin.c \
			externals/libjpeg/jcapistd.c \
			externals/libjpeg/jcarith.c \
			externals/libjpeg/jccoefct.c \
			externals/libjpeg/jccolor.c \
			externals/libjpeg/jcdctmgr.c \
			externals/libjpeg/jchuff.c \
			externals/libjpeg/jcinit.c \
			externals/libjpeg/jcmainct.c \
			externals/libjpeg/jcmarker.c \
			externals/libjpeg/jcmaster.c \
			externals/libjpeg/jcomapi.c \
			externals/libjpeg/jcparam.c \
			externals/libjpeg/jcprepct.c \
			externals/libjpeg/jcsample.c \
			externals/libjpeg/jctrans.c \
			externals/libjpeg/jdapimin.c \
			externals/libjpeg/jdapistd.c \
			externals/libjpeg/jdarith.c \
			externals/libjpeg/jdatadst.c \
			externals/libjpeg/jdatasrc.c \
			externals/libjpeg/jdcoefct.c \
			externals/libjpeg/jdcolor.c \
			externals/libjpeg/jddctmgr.c \
			externals/libjpeg/jdhuff.c \
			externals/libjpeg/jdinput.c \
			externals/libjpeg/jdmainct.c \
			externals/libjpeg/jdmarker.c \
			externals/libjpeg/jdmaster.c \
			externals/libjpeg/jdmerge.c \
			externals/libjpeg/jdpostct.c \
			externals/libjpeg/jdsample.c \
			externals/libjpeg/jdtrans.c \
			externals/libjpeg/jerror.c \
			externals/libjpeg/jfdctflt.c \
			externals/libjpeg/jfdctfst.c \
			externals/libjpeg/jfdctint.c \
			externals/libjpeg/jidctflt.c \
			externals/libjpeg/jidctfst.c \
			externals/libjpeg/jidctint.c \
			externals/libjpeg/jquant1.c \
			externals/libjpeg/jquant2.c \
			externals/libjpeg/jutils.c \
			externals/libjpeg/jmemansi.c \
			externals/libjpeg/jmemmgr.c
EXTRA_CFLAGS:=-DHAVE_STDLIB_H
INCLUDE_DIRS:=externals/libjpeg
EXTRA_DEP:=externals/libjpeg/jconfig.h
SUBFOLDER:=externals/
include make_rules/make_cu_defs.mk
LIBNAME:=lua

SRCS:=$(shell ls externals/lua/src/*.c)
SRCS:=$(filter-out %wmain.c, $(SRCS))
SRCS:=$(filter-out %/luac.c, $(SRCS))
SRCS:=$(filter-out %/loadlib_rel.c, $(SRCS))
INCLUDE_DIRS:=externals/lua/src
EXTRA_DEP:=externals/lua/src/luaconf.h
SUBFOLDER:=externals/
include make_rules/make_cu_defs.mk
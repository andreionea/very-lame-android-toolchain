LIBNAME:=assimp
SRCS:=$(shell find externals/assimp -name \*.cpp -not -path "externals/assimp/test/unit/*" -not -path "externals/assimp/tools/*" -not -path "externals/assimp/samples/*" -not -path "externals/assimp/port/*")
SRCS+=$(shell find externals/assimp -name \*.cc)
SRCS+=externals/assimp/contrib/ConvertUTF/ConvertUTF.c
SRCS:=$(filter-out %Exporter.cpp, $(SRCS))
INCLUDE_DIRS:=externals/assimp/include externals/assimp/code externals/assimp/code/BoostWorkaround
DEFINES:=-DASSIMP_BUILD_NO_OWN_ZLIB -DASSIMP_BUILD_NO_EXPORT
EXTRA_CXXFLAGS:=$(DEFINES) -Wno-c++11-narrowing -Wno-register
EXTRA_CFLAGS:=$(DEFINES)
EXTRA_DEP:=externals/assimp/code/revision.h
SUBFOLDER:=externals/
PCH:=externals/assimp/code/AssimpPCH.h
NUM_CU:=4
EXCLUDES:=%.c %Loader.cpp %Importer.cpp %LWOMaterial.cpp 
include make_rules/make_cu_defs.mk

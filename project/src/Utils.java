package com.andreionea;

import android.util.Log;
import android.app.NativeActivity;
import android.view.Window;
import android.view.View;
import android.widget.Toast;

public class Utils
{
	static NativeActivity s_activity = null;
	public static void SetContext(NativeActivity activity)
	{
		s_activity = activity;
	}
	public static void TestFct()
	{
		Log.d("bubu", "OHAI from Java");
	}
	public static void HideUI(final View view)
	{
		view.setSystemUiVisibility(
		  View.SYSTEM_UI_FLAG_LAYOUT_STABLE
		| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
		| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
		| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
		| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
		| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
	}
	public static void PrintToast(String string)
	{
		final String str = string;
		if (s_activity != null) 
		{
			s_activity.runOnUiThread(new Runnable() {
					public void run() {
						Toast toast = Toast.makeText(s_activity.getApplicationContext(), str, Toast.LENGTH_SHORT);
						toast.show();
					}
				});
		}
	}
	public static void EnableImmersive()
	{
		s_activity.runOnUiThread(new Runnable() {
					public void run() {
						final View view = s_activity.getWindow().getDecorView();
						if (view != null) {
							HideUI(view);
							view.setOnSystemUiVisibilityChangeListener (new View.OnSystemUiVisibilityChangeListener() {
								public void onSystemUiVisibilityChange(int visibility) {
									HideUI(view);
								}
							});
						}
					}
				});
	}

}